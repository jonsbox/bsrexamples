# coding=utf-8
from operator import itemgetter
from datetime import datetime, timedelta
from collections import defaultdict
from math import ceil
from models import db, Log, Settings, RefillRequest, RefillRequestRow


timers = {}
BURST_SALES_MUL = 4
ADD_AX_LAG = 6
PERIOD_FULL = Settings.get_settings('requests_period') or 21


u"""
Демо декоратора и SQL
"""


def log_duration(func):
    def wrapper(*args, **kwargs):
        timer = datetime.now()
        res = func(*args, **kwargs)
        timers[func.__name__] = str(datetime.now() - timer)
        return res

    return wrapper


@log_duration
def sales_for_21_days():
    sql = """
TRUNCATE TABLE tmpavgsales;
WITH pd4 AS(
    -- 0: Вычисление акционных/неакционных периодов
    WITH ptp AS(
      -- Точки, участвующие в акциях
      SELECT
        pdt.promo_id, t.id point_id
      FROM promo_department_through pdt
      JOIN tradepoint t USING (department_id)

      UNION

      SELECT
        prt.promo_id, t.id point_id
      FROM promo_region_through prt
      JOIN tradepoint t USING (region_id)

      UNION

      SELECT
        promo_id, tradepoint_id point_id
      FROM promo_tradepoint_through
    ),
    pcur AS(
      SELECT
        ptp.point_id, r.sku_id, current_date
      FROM promorow r
      JOIN promo p on r.promo_id = p.id
      JOIN ptp ON ptp.promo_id = p.id
      JOIN pointmatrix m ON (m.point_id, m.product_id) = (ptp.point_id, r.sku_id)
      WHERE current_date BETWEEN p.point_start AND p.point_finish - '7 days'::INTERVAL
    ),
    pd0 AS(
      -- Берём те, которые закончились менее, чем за 45 дней до сегодня
      -- (в остальных случаях - как обычно: предыдущие 45 дней)
      SELECT
        ptp.point_id, r.sku_id,
        p.point_start, p.point_finish
      FROM promorow r
      JOIN promo p on r.promo_id = p.id
      JOIN ptp ON ptp.promo_id = p.id
      JOIN pointmatrix m ON (m.point_id, m.product_id) = (ptp.point_id, r.sku_id)
      LEFT JOIN pcur ON (pcur.point_id, pcur.sku_id) = (ptp.point_id, r.sku_id)
      WHERE current_date - p.point_finish BETWEEN -1 AND ({0} - 1)
        AND pcur.point_id ISNULL
    ), pd1 AS(
      -- Берём все более ранние по точке-СКЮ
      -- Нумеруем строки по убыванию начала
      SELECT
        ptp.point_id, r.sku_id,
        p.point_start, p.point_finish
      FROM promorow r
      JOIN promo p on r.promo_id = p.id
      JOIN ptp ON ptp.promo_id = p.id
      JOIN pd0 ON (pd0.point_id, pd0.sku_id) = (ptp.point_id, r.sku_id)
      WHERE p.point_start < pd0.point_start

      UNION ALL

      SELECT * FROM pd0
    ), pd2 AS(
        SELECT
            point_id, sku_id,
            generate_series(current_date - '1 day'::INTERVAL, min(point_start) - '{0} days'::INTERVAL, '-1 day')::DATE AS date
        FROM pd1
        GROUP BY point_id, sku_id

        UNION ALL

        SELECT
            point_id, sku_id,
            generate_series(current_date - '1 day'::INTERVAL, current_date - '{0} days'::INTERVAL, '-1 day')::DATE AS date
        FROM pcur
        GROUP BY point_id, sku_id
    ), wp AS(
        -- Без акций
        SELECT
            point_id, product_id sku_id,
            g.d::DATE AS date,
            row_number() OVER(PARTITION BY m.point_id, m.product_id ORDER BY g.d DESC) AS day
        FROM pointmatrix m
        CROSS JOIN generate_series(current_date - '1 day'::INTERVAL, current_date - '{0} days'::INTERVAL, '-1 day') AS g(d)
        WHERE NOT EXISTS(
            SELECT *
            FROM pd2
            WHERE (m.point_id, m.product_id) = (pd2.point_id, pd2.sku_id)
        )
    ), pd3 AS(
        SELECT
            *,
            row_number() OVER(partition by POINT_ID, SKU_ID ORDER BY date DESC) AS day
        FROM pd2
        WHERE NOT EXISTS(
            SELECT * FROM pd1
            WHERE pd1.point_id = pd2.point_id
                AND pd1.sku_id = pd2.sku_id
                AND pd2.date BETWEEN pd1.point_start AND pd1.point_finish
        )
    )
    SELECT pd3.*, m.date ISNULL AS exists
    FROM pd3
    -- Выкидываем дни, когда товара не было в наличии
    LEFT JOIN missingmatrixproduct m USING (point_id, sku_id, date)
    -- JOIN tradepoint t ON m.point_id = t.id
    WHERE day < {0} + 1 -- AND m.date ISNULL
        -- AND t.calc_reqs

    UNION ALL

    SELECT
        wp.*, m.date ISNULL AS exists
    FROM wp
    -- Выкидываем дни, когда товара не было в наличии
    LEFT JOIN missingmatrixproduct m USING (point_id, sku_id, date)
    /* JOIN tradepoint t ON m.point_id = t.id
    WHERE t.calc_reqs */
    -- WHERE m.date ISNULL
),
t_sale AS(
    -- Продажи за 21 день, когда товар был на полке
    WITH bs AS(
        -- Продажи за ранее выбранный период
        SELECT
            pd4.point_id,
            pd4.sku_id,
            SUM(coalesce(r.amount, 0)) AS amount,
            pd4.day,
            pd4.date,
            pd4.exists
        FROM salerow r
        JOIN sale AS s ON r.sale_id = s.id
        JOIN workday w on s.work_day_id = w.id
        JOIN productseries p on r.product_id = p.id
        JOIN product p3 ON p.product_id = p3.code
        RIGHT JOIN pd4 ON (pd4.point_id, pd4.sku_id, pd4.date) = (w.point_id, p3.sku_id, s.time::DATE)
        WHERE pd4.day < 22
        GROUP BY pd4.point_id, pd4.sku_id, pd4.day, pd4.date, pd4.exists

        UNION ALL

        SELECT pd4.point_id, pd4.sku_id,
            SUM(coalesce(r.amount, 0)) * g.amount AS amount,
            pd4.day,
            pd4.date,
            pd4.exists
        FROM ingredient g
        JOIN product i ON g.ingredient_id = i.code
        JOIN productseries ps ON g.product_id = ps.product_id
        JOIN salerow r ON ps.id = r.product_id
        JOIN sale s ON r.sale_id = s.id
        JOIN workday w ON s.work_day_id = w.id
        RIGHT JOIN pd4 ON (pd4.point_id, pd4.sku_id, pd4.date) = (w.point_id, i.sku_id, s.time::DATE)
        WHERE pd4.day < 22
        GROUP BY pd4.point_id, pd4.sku_id, g.amount, pd4.day, pd4.date, pd4.exists
    )
    -- Объединение составных и ингредиентов
    SELECT
        point_id,
        sku_id,
        day,
        date,
        sum(coalesce(amount, 0)) amount,
        exists
    FROM bs
    GROUP BY point_id, sku_id, day, date, exists
)
INSERT INTO tmpavgsales
SELECT * FROM t_sale
RETURNING *;""".format(PERIOD_FULL)
    rows = db.sql_to_dicts(sql)
    return list(rows)


@log_duration
def check_bursts_45(needs):
    vals = map(lambda ((point, sku), date): "('%s','%s','%s'::DATE)" %
                                            (point, sku, (date + timedelta(days=1)).strftime('%Y.%m.%d')),
               needs.items())
    sql = """
WITH t(point_id, sku_id, date) AS(
    VALUES {0}
), sales AS(
    WITH ps AS(
        SELECT
            t.point_id, t.sku_id, s.time::DATE AS date,
            sum(r.amount) AS amount
        FROM salerow r
        JOIN sale AS s ON r.sale_id = s.id
        JOIN workday w on s.work_day_id = w.id
        JOIN productseries p on r.product_id = p.id
        JOIN product p3 ON p.product_id = p3.code
        JOIN t ON (t.point_id, t.sku_id) = (w.point_id, p3.sku_id)
        WHERE s.time >= t.date - interval'{1} days' AND s.time < t.date
        GROUP BY t.point_id, t.sku_id, 3

        UNION ALL

        SELECT
            t.point_id, t.sku_id, s.time::DATE AS date,
            sum(r.amount) AS amount
        FROM ingredient g
        JOIN product i ON g.ingredient_id = i.code
        JOIN productseries ps ON g.product_id = ps.product_id
        JOIN salerow r ON ps.id = r.product_id
        JOIN sale s ON r.sale_id = s.id
        JOIN workday w ON s.work_day_id = w.id
        JOIN t ON (t.point_id, t.sku_id) = (w.point_id, i.sku_id)
        WHERE s.time >= t.date - interval'{1} days' AND s.time < t.date
        GROUP BY t.point_id, t.sku_id, 3
    )
    SELECT
        point_id, sku_id, date,
        row_number() OVER(PARTITION BY point_id, sku_id ORDER BY date DESC) rn,
        sum(amount) amount
    FROM ps
    GROUP BY point_id, sku_id, date
)
SELECT DISTINCT c.point_id, c.sku_id
FROM sales c
JOIN sales n ON (n.point_id, n.sku_id, n.rn + 1) = (c.point_id, c.sku_id, c.rn)
WHERE c.amount <> 0 AND n.amount / c.amount > {2};""".format(','.join(vals), 45 - PERIOD_FULL, BURST_SALES_MUL)
    res = set(db.execute_sql(sql).fetchall())
    return set(needs.keys()) - res


@log_duration
def get_ax():
    sql = """
TRUNCATE TABLE tmpsalegroup;
WITH s AS(
    SELECT
        m.point_id, p.sku_id,
        sum(r.amount * r.price) amount
    FROM salerow r
    JOIN sale s ON r.sale_id = s.id
    JOIN workday w ON s.work_day_id = w.id
    JOIN productseries ps ON r.product_id = ps.id
    JOIN product p ON ps.product_id = p.code
    JOIN pointmatrix m ON (m.point_id, m.product_id) = (w.point_id, p.sku_id)
    WHERE s.time >= current_date - '28 days'::INTERVAL
        AND s.time < current_date
    GROUP BY m.point_id, p.sku_id
), g AS(
    SELECT
        point_id, sku_id,
        CASE
            WHEN sum(amount) OVER wcur / sum(amount) OVER wtotal < .7 THEN 'A'
            WHEN sum(amount) OVER wcur / sum(amount) OVER wtotal < .95 THEN 'B'
            ELSE 'C'
        END g
    FROM s
    WINDOW
        wcur AS (PARTITION BY point_id ORDER BY amount DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW),
        wtotal AS (PARTITION BY point_id)
)
INSERT INTO tmpsalegroup(point_id, sku_id, abc)
SELECT * FROM g;

WITH t AS(
    SELECT
        w.point_id,
        p.sku_id,
        coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '28 days'::INTERVAL AND s.time < current_date - '21 days'::INTERVAL), 0) w1,
        coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '21 days'::INTERVAL AND s.time < current_date - '14 days'::INTERVAL), 0) w2,
        coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '14 days'::INTERVAL AND s.time < current_date - '7 days'::INTERVAL), 0) w3,
        coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '7 days'::INTERVAL AND s.time < current_date), 0) w4,
        (
            coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '28 days'::INTERVAL AND s.time < current_date - '21 days'::INTERVAL), 0) +
            coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '21 days'::INTERVAL AND s.time < current_date - '14 days'::INTERVAL), 0) +
            coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '14 days'::INTERVAL AND s.time < current_date - '7 days'::INTERVAL), 0) +
            coalesce(sum(r.amount) FILTER (WHERE s.time >= current_date - '7 days'::INTERVAL AND s.time < current_date), 0)
        ) / 4.0 a
    FROM salerow r
    JOIN sale s ON r.sale_id = s.id
    JOIN workday w ON s.work_day_id = w.id
    JOIN productseries ps ON r.product_id = ps.id
    JOIN product p ON ps.product_id = p.code
    WHERE s.time >= current_date - '28 days'::INTERVAL
        AND s.time < current_date
    GROUP BY w.point_id, p.sku_id
), g AS(
    SELECT
        point_id, sku_id,
        -- (|/((w1 - a) ^ 2 + (w2 - a) ^ 2 + (w3 - a) ^ 2 + (w4 - a) ^ 2)) / 2 / a,
        CASE
            WHEN (|/((w1 - a) ^ 2 + (w2 - a) ^ 2 + (w3 - a) ^ 2 + (w4 - a) ^ 2)) / 2 / a < .1 THEN 'X'
            WHEN (|/((w1 - a) ^ 2 + (w2 - a) ^ 2 + (w3 - a) ^ 2 + (w4 - a) ^ 2)) / 2 / a > .25 THEN 'Z'
            ELSE 'Y'
        END g
    FROM t
)
UPDATE tmpsalegroup AS a
    SET xyz = g.g
FROM g
WHERE (g.point_id, g.sku_id) = (a.point_id, a.sku_id);
SELECT point_id, sku_id
FROM tmpsalegroup
WHERE abc = 'A' AND xyz = 'X';"""
    res = db.execute_sql(sql)
    return set(res.fetchall())


@log_duration
def get_shippers_data():
    sql = """
WITH t_sp AS(
  -- выбор приоритетного продукта или с максимальным МРЦ из SKU
  SELECT
    p2.shipper_id, p2.contract_id, p.sku_id, p.code product_id, p2.price,
    p2.multiplicity, c2.time_lag, c2.zapas,
    p2.department_id,
    row_number() OVER (
      PARTITION BY p2.department_id, p2.shipper_id, p2.contract_id, p.sku_id
      ORDER BY p.priority, p2.price DESC
    ) rn
  FROM pointmatrix m
  JOIN product p ON p.sku_id = m.product_id
  JOIN productshipper p2 on p.code = p2.product_id
  JOIN contract c2 on p2.contract_id = c2.code
  WHERE p2.price > 0 AND p2.multiplicity > 0
    AND NOT c2.archived
    AND p2.request_type_id = (
      SELECT id FROM requesttype
      WHERE name = 'ПО УМОЛЧАНИЮ'
    )
)
SELECT shipper_id, contract_id, sku_id, product_id,
price, multiplicity, time_lag, zapas, department_id
FROM t_sp
WHERE rn = 1;"""
    rows = db.sql_to_dicts(sql)
    res = defaultdict(list)
    for r in rows:
        res[(r['sku_id'], r['department_id'])].append(r)
    return res


@log_duration
def get_matrix_data():
    sql = """
WITH reqs AS(
      SELECT r.point_id, p.sku_id, SUM(rr.amount) AS amount
      FROM refillrequestrow AS rr
      JOIN refillrequest AS r ON rr.request_id = r.id
      JOIN product p ON rr.product_id = p.code
      WHERE r.active AND r.confirm_date NOTNULL
      GROUP BY r.point_id, p.sku_id
)
SELECT
    m.point_id, m.product_id, m.minimum,
    sum(b.balance) balance,
    reqs.amount
FROM pointbalance b
JOIN productseries s ON b.product_id = s.id
JOIN product p ON s.product_id = p.code
RIGHT JOIN pointmatrix m ON (m.point_id, m.product_id) = (b.point_id, p.sku_id)
LEFT JOIN reqs ON (reqs.point_id, reqs.sku_id) = (m.point_id, m.product_id)
GROUP BY m.point_id, m.product_id, m.minimum, reqs.amount;"""
    return {(r[0], r[1]): {
        'mtz': r[2],
        'balance': r[3],
        'requested': r[4]
    } for r in db.execute_sql(sql).fetchall()}


def get_points_info():
    period = Settings.get_settings('period_days') or 30.0
    sql = """
WITH t AS(
    SELECT
        id, department_id
    FROM tradepoint
    WHERE NOT archived AND calc_reqs
)
SELECT
    t.id, t.department_id,
    coalesce(current_date - min(s.time)::date, 0) work_days
FROM t
LEFT JOIN workday w ON w.point_id = t.id
LEFT JOIN sale s ON w.id = s.work_day_id
WHERE s.time >= current_date - interval'{} days' OR s.time ISNULL
GROUP BY t.id, t.department_id;""".format(period)
    return {r[0]: {
        'department': r[1],
        'work_days': r[2]
    } for r in db.execute_sql(sql).fetchall()}
