# coding=utf-8
import codecs
import csv
import json
import os
import shutil
import smtplib
import traceback
import xmltodict
from collections import defaultdict
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from ftplib import FTP, error_perm
from io import BytesIO
from operator import itemgetter
from platform import system as system_name
from socket import error as conn_err
from subprocess import call
from zipfile import BadZipfile, ZipFile
from peewee import CompositeKey, IntegrityError, fn, JOIN_LEFT_OUTER as LJ

from models import db, Log, RequestType, Contract, ProductShipper, Country, OKEI, ProductGroup, SKUGroup, SKU, \
    PriceGroup, NDS, DiscountSegment, ProductType, Product, ProductSeries, BarCode, Price, Ingredient, DiscountTrigger, \
    DTRuleTime, DTRuleProduct, DiscountTriggerPoint, DiscountGroup, Discount, DiscountRule, SellToOrderPack, \
    RefillRequest, TradePoint, DiscountInfoCard, DiscountCardType
from mainrequests.main import calc_reqs
from mainrequests.beta import stock_requests
from settings import BASE_DIR, FTP_PATH, FTP_HOST, FTP_PORT, FTP_LOGIN, FTP_PASS, PRODUCT_PHOTOS, MAIL_SERVER, \
    MAIL_PORT, MAIL_SENDER, MAIL_PASS, ONES_PATH


u"""
Демо адапетра
"""


class FTPError(BaseException):
    pass


class BSFTP(object):
    """
    Класс для скачивания файлов xml из 1С
    """


class RowGenerator(object):
    """
    Генератор строки для вставки в БД из узла xml
    """
    @staticmethod
    def _raw(row, val):
        v = row[val]
        return 'NULL' if v is None else u'{}'.format(v)

    @staticmethod
    def _nraw(row, val):
        v = row.get(val, None)
        return 'NULL' if v is None else u'{}'.format(v)

    @staticmethod
    def _db_quote(_v):
        return _v.replace('%', '%%').replace("'", "''")

    @classmethod
    def _quote(cls, row, val):
        v = row[val]
        return 'NULL' if v is None else u"'{}'".format(cls._db_quote(v))

    @classmethod
    def _nquote(cls, row, val):
        v = row.get(val, None)
        return 'NULL' if v is None else u"'{}'".format(cls._db_quote(v))

    @staticmethod
    def _bool(row, val):
        v = row[val]
        if v is None:
            return 'NULL'
        else:
            return str(v == u'ИСТИНА')

    @staticmethod
    def _nbool(row, val):
        v = row.get(val, None)
        if v is None:
            return 'NULL'
        else:
            return str(v == u'ИСТИНА')

    @staticmethod
    def _const(row, val):
        return val

    def __init__(self, *args):
        """
        На входе - список тэгов с префиксами:
        нет - в сыром виде (например, целое)
        '   - в кавычки (строка, дата)
        |   - булево
        ?   - может не быть (вместо r['x'] будет применяться r.get('x', None)
        *   - константа
        (adapter, tag) - применить функцию (или метод) adapter,
            где adapter(row, tag) -> object,
            здесь:
            row - исходный элемент (узел) xml
            tag - тэг, на который возбудился обработчик
            object - значение для вставки в БД ("'{1,2,3}'")

        :param args:
        """
        handlers, params = [], ['{}'] * len(args)
        self._pattern = u'(%s)' % u','.join(params)
        for arg in args:
            params.append('%s')
            arg_type = arg[0]
            if isinstance(arg, tuple):
                func = arg[0].__func__
                tag = arg[1]
            elif arg_type == "'":
                if arg[1] == '?':
                    func = self._nquote
                    tag = arg[2:]
                else:
                    func = self._quote
                    tag = arg[1:]
            elif arg_type == '|':
                if arg[1] == '?':
                    func = self._nbool
                    tag = arg[2:]
                else:
                    func = self._bool
                    tag = arg[1:]
            elif arg_type == '*':
                func = self._const
                tag = arg[1:]
            else:
                if arg_type == '?':
                    func = self._nraw
                    tag = arg[1:]
                else:
                    func = self._raw
                    tag = arg
            handlers.append((tag, func))
        self._handlers = tuple(handlers)

    def row(self, line):
        vals = map(lambda (t, f): f(line, t), self._handlers)
        return self._pattern.format(*vals)


class ModelUpdater1s(object):
    """
    Обновление (вставка новых данных или изменение существующих) данных по моделям
    из узла xml
    """
    @staticmethod
    def _get_pk(model):
        meta = model.get_meta()
        pk = meta.primary_key
        if isinstance(pk, CompositeKey):
            pk = map(lambda attr: '"%s"' % getattr(model, attr).db_column, pk.field_names)
            pk = ','.join(pk)
        else:
            pk = '"%s"' % pk.db_column
        return pk

    @staticmethod
    def _get_column_pars(model):
        tags, cols, updateable = [], [], []
        for field in model.pars_1s:
            tags.append(field[1])
            f = getattr(model, field[0]).db_column
            cols.append(f)
            if field[2]:
                updateable.append(f)
        return tags, cols, updateable

    @staticmethod
    def _prepare_sql(table, pk, cols, updateable):
        arr_set, arr_where_l, arr_where_r = [], [], []
        for c in updateable:
            arr_set.append(u"{0}=EXCLUDED.{0}".format(c))
            arr_where_l.append(u"t.%s" % c)
            arr_where_r.append(u"EXCLUDED.%s" % c)
        if arr_where_l:  # Таблица не опустошается, возможны конфликты
            sql = u"""
INSERT INTO %s AS t(%s)
VALUES %%s
ON CONFLICT (%s) DO UPDATE SET
  %s
WHERE (%s) IS DISTINCT FROM (%s);""" % (table, u','.join(cols), pk, u','.join(arr_set),
                                        u','.join(arr_where_l), u','.join(arr_where_r))
        else:  # Таблица опустошается, конфликтов не будет
            sql = u"""
INSERT INTO %s AS t(%s)
VALUES %%s;""" % (table, u','.join(cols))
        return sql

    def __init__(self, model):
        pk = self._get_pk(model)
        self._pk = model.pars_1s[0][1]  # тэг ПК
        if self._pk[0] == "'":
            self._pk = self._pk[1:]
        table = model.get_meta().db_table
        tags, cols, updateable = self._get_column_pars(model)
        self._sql = self._prepare_sql(table, pk, cols, updateable)
        self._generator = RowGenerator(*tags)

    def update(self, data):
        rows, done = [], set()
        pk = self._pk
        for r in data:
            k = r[pk]
            if k not in done:
                done.add(k)
                rows.append(self._generator.row(r))
        sql = self._sql % u','.join(rows)
        db.execute_sql(sql)


def process_xml_ktg(data, prods, hourly):
    """
    Пример использования ModelUpdater1s
    :param data:
    :param prods:
    :param hourly:
    :return:
    """

    def update_reqtypes(_data):
        _updater = ModelUpdater1s(RequestType)
        if type(_data) != list:
            _data = [_data]
        _updater.update(_data)

    ProductShipper.delete().execute()
    if data[u'Справочник.СпособПополнения']:
        update_reqtypes(data[u'Справочник.СпособПополнения'][u'Элемент'])
