# coding=utf-8
import inspect
import json
import os
from hashlib import sha256
from datetime import datetime, date, timedelta
from uuid import uuid4
from operator import itemgetter
from psycopg2.extensions import TransactionRollbackError
from peewee import fn, SQL, Model, CharField, BooleanField, ForeignKeyField, DoubleField,\
    TextField, DecimalField, CompositeKey, transaction, SmallIntegerField,\
    DateField, DateTimeField, IntegerField, FloatField, Field, TimeField, OperationalError, InterfaceError
from playhouse.postgres_ext import PostgresqlExtDatabase, JSONField, DateTimeTZField, ArrayField, UUIDField
from playhouse.fields import ManyToManyField, ManyToManyFieldDescriptor
from settings import DB_NAME, DB_USER, DB_PASSWORD, HASH_SALT, BASE_DIR


MAX_PHONE_LEN = 64


u"""
Демо контекстов
"""


class BSPGTransaction(transaction):
    """
    Класс для транзацкий разного уровня
    (пока для REPEATABLE READ)
    """

    def _begin(self):
        self.db.execute_sql('BEGIN ISOLATION LEVEL REPEATABLE READ;')


class AtTZQueriesContext(object):
    """
    Транзакция в указанном часовом поясе
    """

    _tz = 0
    _db = None

    def __init__(self, _db, tz=0):
        super(AtTZQueriesContext, self).__init__()
        self._tz = int(tz)
        self._db = db

    def __enter__(self):
        self._db.execute_sql('SET LOCAL TIME ZONE +%d;' % int(self._tz))

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            self._db.execute_sql('SET LOCAL TIME ZONE DEFAULT;')
        except BaseException:
            pass


class WithVarQueriesContext(object):
    """
    Транзакция с локальной переменной (point, trader и т.д.)
    Получить значние - bs_var(имя text) as text
    Пример: Log.user == fn.bs_var(SQL("'bsuser'")).cast('int')
    """

    _var = None
    _val = None
    _db = None

    def __init__(self, _db, var, val):
        super(WithVarQueriesContext, self).__init__()
        self._var = var
        self._val = val
        self._db = db

    def __enter__(self):
        self._db.execute_sql(u"SET LOCAL \"my.vars.%s\" = '%s';" % (self._var, self._val))

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class BSPGDatabase(PostgresqlExtDatabase):
    def execute_sql(self, sql, params=None, require_commit=True):
        try:
            cursor = super(PostgresqlExtDatabase, self).execute_sql(
                sql, params, require_commit)
        except (OperationalError, InterfaceError):  # переподключение при разрыве появилось в новых версиях
            if not self.is_closed():
                self.close()
            with self.exception_wrapper:
                cursor = self.get_cursor()
                cursor.execute(sql, params or ())
                if require_commit and self.get_autocommit():
                    self.commit()
        return cursor

    def trans_rr(self, transaction_type=None):
        """

        :param transaction_type:
        :return:
        Выполнить как транзакцию уровня REPEATABLE READ
        """
        return BSPGTransaction(self, transaction_type)

    def try_trans_rr(self, func, f_args=None, f_kwargs=None, times=10):
        """

        :param func: выполняемая фукиця
        :param f_args: неименованные аргументы
        :param f_kwargs: именованные аргументы
        :param times: число попыток
        :return: результат выполенеия func
        :exception psycopg2.extensions.TransactionRollbackError:
            при превышении числа попыток

        Попытаться выполнить функцию в транзакции уровня REPEATABLE READ
            с повтором при неудаче указанное число раз
        """
        res = None
        args = f_args or ()
        kwargs = f_kwargs or {}
        while times:
            try:
                with self.trans_rr():
                    res = func(db, *args, **kwargs)
                    break
            except TransactionRollbackError as e:
                res = e
                times -= 1
        if type(res) == TransactionRollbackError:
            raise res
        return res

    def trans_at_timezone(self, tz=0):
        return AtTZQueriesContext(self, tz)

    def trans_with_var(self, var, val):
        return WithVarQueriesContext(self, var, val)

    def trans_with_bsuser(self, user):
        return self.trans_with_var('user', user)

    def trans_with_bspoint(self, point):
        return self.trans_with_var('point', point)

    def trans_with_bstrader(self, trader):
        return self.trans_with_var('trader', trader)

    def sql_to_dicts(self, sql):
        cur = self.execute_sql(sql)
        cols = map(itemgetter(0), cur.description)
        for r in cur.fetchall():
            yield dict(zip(cols, r))


db = BSPGDatabase(
    DB_NAME,
    user=DB_USER,
    password=DB_PASSWORD,
    autocommit=True,
    autorollback=True,
    register_hstore=False,
)


def StrDefault(value):
    return SQL("DEFAULT '%s'" % value)


def RowDefault(value):
    return SQL("DEFAULT %s" % str(value))


class SerialField(Field):
    field_type = 'bigserial'
    db_field = 'bigserial'

    def db_value(self, value):
        return int(value)

    def python_value(self, value):
        return int(value)


class BaseModel(Model):
    """

    """
    @classmethod
    def get_meta(cls):
        return cls._meta

    class Meta:
        database = db


class Base1SModel(BaseModel):
    id = UUIDField(primary_key=True, default=uuid4)


class MyCharField(CharField):
    def __init__(self, *args, **kwargs):
        kwargs['null'] = True
        super(MyCharField, self).__init__(*args, **kwargs)


class Country(BaseModel):
    pars_1s = (
        ('code', u"'Код", False),
        ('name', u"'Наименование", True),
        ('sign', u"'КодАльфа2", True)
    )

    code = CharField(primary_key=True, max_length=4)
    name = CharField()
    sign = CharField(max_length=3, null=True)


class Organization(BaseModel):
    @staticmethod
    def nalog_from_row(row, nalog_sys_tag):
        nalog_sys = row[nalog_sys_tag]
        res = []
        if row[u'РозничнаяТорговляОблагаетсяЕНВД'] == u'ИСТИНА':
            res.append('8')
        if nalog_sys == u'Упрощенная':
            usn_obj = row[u'ОбъектНалогообложенияУСН']
            if usn_obj == u'Доходы':
                res.append('2')
            elif usn_obj == u'ДоходыМинусРасходы':
                res.append('4')
        elif nalog_sys == u'Общая':
            res.append('1')
        return "'{%s}'" % ','.join(res)

    pars_1s = (
        ('id', u"'Код", False),
        ('inn', u"'ИНН", True),
        ('kpp', u"'КПП", True),
        ('name', u"'Наименование", True),
        ('full_name', u"'НаименованиеПолное", True),
        ('address', u"'Адрес", True),
        ('without_nds', u"|ПрименяетсяОсвобождениеОтУплатыНДС", True),
        ('nalog', (nalog_from_row, u"СистемаНалогообложения"), True)
    )

    id = CharField(max_length=9, primary_key=True)
    inn = CharField(max_length=12)
    kpp = CharField(null=True, max_length=9)
    name = CharField()
    full_name = CharField()
    address = CharField(null=True)
    without_nds = BooleanField()
    nalog = ArrayField(SmallIntegerField)


class Department(BaseModel):
    pars_1s = (
        ('id', u'Код', False),
        ('name', u"'Наименование", True),
        ('gmt', '*0', False),
        ('time_lag', '*0', False),
    )

    name = CharField()
    parent = ForeignKeyField('self', null=True)
    gmt = SmallIntegerField(default=0)
    time_lag = SmallIntegerField(default=0)
    archived = BooleanField(default=False)


class Position(BaseModel):
    name = CharField()
    parent = ForeignKeyField('self', null=True)
    accept_request = BooleanField(default=False)


class User(BaseModel):
    login = CharField(unique=True, max_length=20)
    email = CharField(max_length=64, null=True)
    pwd = CharField(max_length=64)
    name = CharField()
    departments = ManyToManyField(Department, related_name='department_users')
    positions = ManyToManyField(Position, related_name='position_users')
    photo = CharField(null=True)
    phone = CharField(null=True, max_length=32)
    active = BooleanField(default=True)

    @staticmethod
    def create_hash(login, password):
        s = u'%s%s%s' % (login, HASH_SALT, password)
        r = sha256(s.encode('utf8')).hexdigest()
        return r


# Права доступа


class Permission(BaseModel):
    position = ForeignKeyField(Position, null=True, on_delete='CASCADE')
    user = ForeignKeyField(User, null=True, on_delete='CASCADE')
    entity_type = CharField(max_length=8, choices=(
        ('catalog', u'Справочники'),
        ('document', u'Документы'),
        ('report', u'Отчёты'),
        ('other', u'Прочее')
    ))
    entity_name = CharField()
    level = CharField(max_length=4, choices=(
        ('view', u'Просмотр'),
        ('full', u'Полный доступ')
    ))

    class Meta:
        constraints = [SQL('CHECK(position_id NOTNULL OR user_id NOTNULL)')]
