# coding=utf-8
import json
import traceback
import cgi
import logging
import os
import base64
import time
import re
import urllib
import calendar
from io import BytesIO
from psutil import Process
from time import sleep
from datetime import datetime, timedelta, date, time
from operator import attrgetter, itemgetter
from uuid import UUID, uuid4
from collections import defaultdict, OrderedDict
from decimal import Decimal
from threading import Thread
from random import randint
from peewee import IntegrityError, DataError, SelectQuery, InternalError, JOIN_LEFT_OUTER as LJ, JOIN, fn, SQL
from playhouse.shortcuts import model_to_dict
from tornado.escape import utf8
from tornado.web import RequestHandler, ErrorHandler, HTTPError, MissingArgumentError, \
    _time_independent_equals
from tornado.websocket import WebSocketHandler, WebSocketClosedError
from tornado.gen import coroutine
from tornado.iostream import StreamClosedError
from openpyxl import Workbook
from openpyxl.styles import Font, Alignment
from tornado.escape import json_encode
from models import db, Log, User, Department, Position, TradePoint, TPUser, ChatRoom, ChatRoomMember, ChatMessage,\
    ChatMessageReciever, ProductGroup, Product, OKEI, Request, Directive, DirectiveRecievers, Permission, Shipper,\
    ProductShipper, Settings, Contract, MAX_PHONE_LEN, POSUpdate, CalendarEvent, RefillRequestEvent as ReqEvent,\
    ShipperUser, ShipperEvent, RefillRequestEvent, Sale, WorkDay, Region, RegionManager, POSLog, ZReport, POSDocument
from settings import USER_PHOTOS, BASE_DIR, EXPIRE_COOKIE, APP_DIR, POS_DOCS_DIR, POS_APP_DIR
from excelbuilder import WBBuilder


u"""
Демо декораторов и реализации прав доступа, т.к. в этой версии tornado их нет
"""


logger = logging.getLogger("handlers")
RJ = JOIN.RIGHT_OUTER
json_old_serializer = json.JSONEncoder.default


def json_serializer(self, obj):
    if isinstance(obj, datetime):
        return obj.strftime('%Y.%m.%d %H:%M:%S')
    elif isinstance(obj, date):
        return obj.strftime('%Y.%m.%d')
    elif isinstance(obj, UUID):
        return str(obj)
    elif isinstance(obj, Decimal):
        return float(obj)
    elif isinstance(obj, SelectQuery):
        return list(obj)
    elif isinstance(obj, time):
        return str(obj)
    elif isinstance(obj, set):
        return list(obj)
    else:
        return json_old_serializer(self, obj)


json.JSONEncoder.default = json_serializer


def datetime_to_int(dt):
    return time.mktime(dt.timetuple()) * 1000 + dt.microsecond / 1000


class ProtoHandler(RequestHandler):
    _user = None

    def get_current_user(self):
        return self._user

    def set_current_user(self, user):
        self._user = user

    def add_to_log(self, **kwargs):
        kwargs['user'] = self._user
        Log.create(**kwargs)

    def debug(self, data):
        self.add_to_log(
            level='debug',
            type='system',
            event='debug',
            event_pars={
                'data': data
            }
        )

    def check_xsrf_cookie(self):
        pass

    def _handle_request_exception(self, e):
        # Переопределить исключение self.send_error(e.status_code, exc_info=sys.exc_info())
        # Подумать над заменой на def log_exception(self, typ, value, tb):
        super(ProtoHandler, self)._handle_request_exception(e)
        if isinstance(e, (HTTPError, StreamClosedError)):
            return
        try:
            data = self.request.body
            if self.request.files or not isinstance(data, basestring):
                data = '-- binary data --'
            else:
                try:
                    data = data.decode('utf8')
                except UnicodeDecodeError as e:
                    try:
                        data = data.decode('cp1251')
                    except UnicodeEncodeError:
                        data = '-- unknown data coding --'
            if len(data) > 2000:
                data = u'%s\\n...\\n%s' % (data[:999], data[-999:])
            self.add_to_log(
                level='critical',
                type='system',
                event=u'Необработанное исключение',
                event_pars={
                    'request': self.request.uri,
                    'user_agent': self.request.headers.get('User-Agent', '-'),
                    'data': data,
                    'exception': unicode(e),
                    'stacktrace': traceback.format_exc()
                }
            )
        except BaseException:
            pass

    def on_finish(self):
        try:
            warn_mem = 1073741824
            pid = os.getpid()
            process = Process(pid)
            mem = process.memory_info().rss
            if mem > warn_mem:
                fname = os.path.join(BASE_DIR, 'big_posts')
                if not os.path.exists(fname):
                    os.makedirs(fname)
                fname = os.path.join(fname, str(pid))
                if os.path.exists(fname):
                    return
                with open(fname, 'wb') as f:
                    f.write(self.request.body)
                self.add_to_log(
                    level='critical',
                    type='system',
                    event=u'Превышение лимита памяти',
                    event_pars={
                        'request': self.request.uri,
                        'user_agent': self.request.headers.get('User-Agent', '-'),
                        'post_data': fname,
                        'memory': mem
                    }
                )
        except BaseException:
            pass


class BaseHandler(ProtoHandler):
    _groups = None

    def get_current_user(self):
        if self.get_secure_cookie("user"):
            return unicode(self.get_secure_cookie("user").replace("\"", ""), 'unicode-escape')
        else:
            return None

    def get_user_object(self):
        if self._user:
            return self._user

        if self.get_secure_cookie("user"):
            try:
                user = User.get(User.id == self.get_current_user())
                self._user = user
                self._groups = list(user.positions)
                return user
            except User.DoesNotExist:
                return None
        else:
            return None

    def not_authorized(self, *args, **kwargs):
        if self._user is None:
            self.redirect('/login')
        else:
            raise HTTPError(403)

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)
        user = self.get_user_object()
        if user is None or not user.active:
            self.get = self.post = self.not_authorized
        else:
            pass

    def is_admin(self):
        return 'admin' in map(attrgetter('name'), self._groups)

    def error(self, error):
        self.write({
            'result': False,
            'reason': error
        })

    def ok(self, data=None):
        res = {'result': True}
        if data is not None:
            res['data'] = data
        self.write(res)

    @property
    def my_positions(self):
        return self._groups


# Декораторы для прав доступа


def perm_required(func):
    def has_permission(user, cls):
        if not user:
            return False
        if user.login == 'dev' or 'admin' in map(attrgetter('name'), list(user.positions)):
            return True
        try:
            perm = Permission.select(Permission.level).where(
                (Permission.entity_type == cls.entity_type) &
                (Permission.entity_name == cls.entity_name) &
                (
                    (Permission.user == user.id) |
                    (Permission.position << map(attrgetter('id'), list(user.positions)))
                )
            ).order_by(Permission.user).limit(1).tuples()[0][0]
            # сортировка по юзеру важна, т.к. у юезра более выскоий приотритет
            if cls.entity_level == 'full':
                return perm == 'full'
            else:
                return True
        except (AttributeError, IndexError):
            return False

    def not_permitted():
        raise HTTPError(403)

    def wrapper(*args, **kwargs):
        cls = args[0]
        user = cls.get_user_object()
        return func(*args, **kwargs) if has_permission(user, cls) else not_permitted()

    return wrapper


def admin_required(func):
    def has_permission(user, cls):
        if not user:
            return False
        if user.login == 'dev' or 'admin' in map(attrgetter('name'), list(user.positions)):
            return True
        else:
            return False

    def not_permitted():
        raise HTTPError(403)

    def wrapper(*args, **kwargs):
        cls = args[0]
        user = cls.get_user_object()
        return func(*args, **kwargs) if has_permission(user, cls) else not_permitted()

    return wrapper


# Подразделения
class DepartmentsHandler(BaseHandler):
    entity_type = 'catalog'
    entity_name = 'department'
    entity_verbose = u'Подразделения'
    entity_level = 'view'

    @perm_required
    def get(self, *args, **kwargs):
        self.ok(list(Department.select().dicts()))
